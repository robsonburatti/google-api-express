// const express = require('express');

// const app = express();

// const port = process.env.PORT;

// app.get('/', (req, res) => {
//   res.status(200).send({
//     success: 'true',
//     message: 'Seja Bem-Vindo(a) ao mundo Docker!',
//     version: '1.0.0',
//   });
// });

// app.listen(port);
// console.log(`Aplicação executando na porta..: ${port}`);

/////////////////////////////////////////////////////////////////////////////

// importando os pacotes para uso no arquivo index.js
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');

// crio um servidor express
const app = express();

// aplico configurações para dentro do servidor express, adicionando middlewares (body-parser, morgan, cors)
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// DB local (tempo de execução)
const data = [];

// criação de rota que será acessada utilizando o método HTTP GET/
// http://localhost:9000/
app.get('/', (req, res) => {
  console.log('testes passou fome');
  return res.json({ "data2":"mentirawww" });
  
});

// criação de rota que será acessada utilizando o método HTTP POST/
// http://localhost:9000/add
app.post('/add', (req, res) => {
  const result = req.body;

  if (!result) {
    return res.status(400).end();
  }

  data.push(result);
  return res.json({ result });
});

// o servidor irá rodar dentro da porta 9000
app.listen(3000, () => console.log('Express changed at http://localhost:3000'));

